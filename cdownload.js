var downloadButton = document.getElementById("cdl");
var counter = 120;
var newElement = document.createElement("strong");
newElement.innerHTML = "Please Wait! You can download the file in 120 seconds.";
var id;
downloadButton.parentNode.replaceChild(newElement, downloadButton);
id = setInterval(function() {
counter--;
if(counter < 0) {
newElement.parentNode.replaceChild(downloadButton, newElement);
clearInterval(id);
} else {
newElement.innerHTML = "Please Wait! You can download the file in " + counter.toString() + " seconds.";
}}, 1000);